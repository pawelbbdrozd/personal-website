import Typography from 'typography'
import grandViewTheme from 'typography-theme-grand-view'

grandViewTheme.googleFonts = [
  {
    name: 'Comfortaa',
    styles: ['300'],
  },
  {
    name: 'Sanchez',
    styles: ['400'],
  },
  {
    name: 'Arvo',
    styles: ['400', '400i', '700'],
  },
]
grandViewTheme.headerFontFamily = ['Comfortaa', 'sans-serif']

const typography = new Typography(grandViewTheme)

export default typography
