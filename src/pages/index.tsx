import React from 'react'

import Layout from '@/components/layout'
import Hero from '@/components/hero'

interface IndexPageProps {
  __seoData?: Record<string, any>
}
const IndexPage: React.FC<IndexPageProps> = props => (
  <Layout __seoData={props.__seoData}>
    <Hero
      greeting="Hello I'm Paweł"
      info={[
        '&#60; Fullstack Web Engingeer /&#62;',
        '&#60; Fullstack Mobile Engingeer /&#62;',
      ]}
    />
  </Layout>
)

export default IndexPage
