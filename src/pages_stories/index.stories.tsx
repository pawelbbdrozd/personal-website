import React from 'react'
import { storiesOf } from '@storybook/react'
import IndexPage from '../pages/index'

const seoData = {
  site: {
    siteMetadata: {
      title: 'Default title',
      description: 'Default description',
      author: 'Default author',
    },
  },
}

storiesOf(`pages.Index`, module).add(`default`, () => (
  <IndexPage __seoData={seoData} />
))
