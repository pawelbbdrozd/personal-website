import React from 'react'
// import { graphql, useStaticQuery } from 'gatsby'
import './layout.css'

import Hero from '@/components/hero'
import Nav from '@/components/nav'
import SEO from '@/components/seo'

interface LayoutProps {
  __seoData?: any
}

const Layout: React.FC<LayoutProps> = ({ __seoData, children }) => {
  return (
    <div>
      <SEO __data={__seoData} />
      <Nav />
      {children}
    </div>
  )
}

export default Layout
