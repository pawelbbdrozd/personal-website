import * as React from 'react'
import style from './hero.module.css'
import posed from 'react-pose'
import { useEffect, useState } from 'react'
import Typed from 'react-typed'

interface HeroProps {
  greeting: string
  info: string[]
}

const Greeting = posed.div({
  hidden: { x: '60vw' },
  shown: { x: 0, transition: { duration: 500 } },
  exit: { x: '-110vw', transition: { duration: 900 } },
})

const Hero: React.FC<HeroProps> = ({ greeting, info }) => {
  const [greetingsVisible, setGreetingsVisible] = useState(false)
  const [showInfo, setShowInfo] = useState(false)
  const [exit, setExit] = useState(false)

  const getGreetingsPoseClass = (
    greetingsVisible: boolean,
    exit: boolean
  ): string => {
    if (exit) return 'exit'
    if (!greetingsVisible) return 'hidden'
    else return 'shown'
  }

  useEffect(() => {
    const scrollHandler = () => {
      if (window.scrollY >= 200) {
        setExit(true)
      } else if (window.scrollY <= 50) {
        setExit(false)
      }
    }
    window.addEventListener('scroll', scrollHandler)
    setTimeout(() => setGreetingsVisible(true), 800)
    setTimeout(() => setShowInfo(true), 1400)
    return () => window.removeEventListener('scroll', scrollHandler)
  }, [])

  return (
    <header className={style.hero}>
      <div className={style.left} />
      <div className={style.right}>
        <div className={style.heroTextContainer}>
          <Greeting
            className={style.greetingWrapper}
            pose={getGreetingsPoseClass(greetingsVisible, exit)}
          >
            <h1 className={style.greeting}>{greeting}</h1>
          </Greeting>
          <h2 className={style.info}>
            {showInfo && (
              <Typed
                strings={info}
                typeSpeed={50}
                backSpeed={60}
                smartBackspace
                loop
              />
            )}
          </h2>
        </div>
      </div>
    </header>
  )
}

export default Hero
