const path = require('path')
const {TypedCssModulesPlugin} = require('typed-css-modules-webpack-plugin')
const gatsbyConfig = require('../gatsby-config')
const postCssPlugins = gatsbyConfig.plugins.find(entry => entry.resolve === `gatsby-plugin-postcss`).options.postCssPlugins

module.exports = ({config}) => {
  // Transpile Gatsby module because Gatsby includes un-transpiled ES6 code.
  config.module.rules[0].exclude = [/node_modules\/(?!(gatsby)\/)/]

  // use installed babel-loader which is v8.0-beta (which is meant to work with @babel/core@7)
  config.module.rules[0].use[0].loader = require.resolve('babel-loader')

  // use @babel/preset-react for JSX and env (instead of staged presets)
  config.module.rules[0].use[0].options.presets = [
    require.resolve('@babel/preset-react'),
    require.resolve('@babel/preset-env'),
  ]

  // use @babel/plugin-proposal-class-properties for class arrow functions
  config.module.rules[0].use[0].options.plugins = [
    require.resolve('@babel/plugin-proposal-class-properties'),
  ]

  // Prefer Gatsby ES6 entrypoint (module) over commonjs (main) entrypoint
  config.resolve.mainFields = ['browser', 'module', 'main']

  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    loader: require.resolve('babel-loader'),
    options: {
      presets: [['react-app', {flow: false, typescript: true}]],
    },
  })
  config.resolve.extensions.push('.ts', '.tsx')
  config.resolve.alias['@'] = path.resolve('src')

  const rules = config.module.rules
  config.module.rules[2] = {
    oneOf: [
      {
        test: /\.module\.css$/,
        use: [
          {
            options: {hmr: false},
            loader:
              'style-loader',
          },
          {
            loader:
              'css-loader',
            options: {
              sourceMap: true,
              camelCase: 'dashesOnly',
              localIdentName: '[name]--[local]--[hash:base64:5]',
              importLoaders: 1,
              modules: true,
            },
          },
          {
            loader:
              'postcss-loader',
            options: {
              sourceMap: true,
              plugins: postCssPlugins,
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [
          {
            options: {},
            loader:
              'style-loader',
          },
          {
            loader:
              'css-loader',
            options: {
              sourceMap: true,
              camelCase: 'dashesOnly',
              localIdentName: '[name]--[local]--[hash:base64:5]',
              importLoaders: 1,
            },
          },
          {
            loader:
              'postcss-loader',
            options: {
              sourceMap: true,
              plugins: postCssPlugins,
            },
          },
        ],
      },
      rules[2],
    ],
  }

  config.plugins.push(
    new TypedCssModulesPlugin({
      globPattern: 'src/**/*.css',
    }),
  )
  return config
}
