import { configure } from '@storybook/react';
import React from 'react';
import {addDecorator} from '@storybook/react';
import typography from '../src/theme/typography'

const TypographyDecorator = (storyFn) => {
  typography.injectStyles();
  let fontsStr = ""
  if (typography.options.googleFonts) {
    const fonts = typography.options.googleFonts.map(font => {
      let str = ""
      str += font.name.split(" ").join("+")
      str += ":"
      str += font.styles.join(",")

      return str
    })

    fontsStr = fonts.join("|")

    if (fontsStr && document.getElementById("typography.js-fonts")) {
        const fontsNode = document.getElementById("typography.js-fonts")
        fontsNode.setAttribute('href', `//fonts.googleapis.com/css?family=${fontsStr}`)
        fontsNode.setAttribute('rel', `stylesheet`)
        fontsNode.setAttribute('type', `text/css`)
    }
  }


  return storyFn();
}

addDecorator(TypographyDecorator);

// automatically import all files ending in *.stories.js
const req = require.context("../src", true, /.stories.[tj]sx?$/)
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

// Gatsby's Link overrides:
// Gatsby defines a global called ___loader to prevent its method calls from creating console errors you override it here
global.___loader = {
  enqueue: () => {
  },
  hovering: () => {
  },
}
// Gatsby internal mocking to prevent unnecessary errors in storybook testing environment
global.__PATH_PREFIX__ = ""
// This is to utilized to override the window.___navigate method Gatsby defines and uses to report what path a Link would be taking us to if it wasn't inside a storybook
window.___navigate = pathname => {
  action("NavigateTo:")(pathname)
}
configure(loadStories, module)

